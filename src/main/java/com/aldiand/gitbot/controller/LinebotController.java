package com.aldiand.gitbot.controller;

import com.aldiand.gitbot.LineBot.MessageSender;
import com.aldiand.gitbot.model.db.Group;
import com.aldiand.gitbot.repository.GroupRepository;
import com.aldiand.gitbot.repository.UserRepository;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.client.MessageContentResponse;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.*;
import com.linecorp.bot.model.event.message.*;
import com.linecorp.bot.model.event.source.GroupSource;
import com.linecorp.bot.model.event.source.RoomSource;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.*;
import java.util.concurrent.ExecutionException;

@Slf4j
@LineMessageHandler
public class LinebotController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private MessageSender messageSender;

    @EventMapping
    public void handleTextMessageEvent(MessageEvent<TextMessageContent> event) throws Exception {
        TextMessageContent message = event.getMessage();
        handleTextContent(event.getReplyToken(), event, message);
    }

    @EventMapping
    public void handleUnfollowEvent(UnfollowEvent event) {
        log.info("unfollowed this bot: {}", event);
    }

    @EventMapping
    public void handleFollowEvent(FollowEvent event) {
        String replyToken = event.getReplyToken();
        messageSender.followMessage(replyToken);
    }

    @EventMapping
    public void handleJoinEvent(JoinEvent event) {
        String replyToken = event.getReplyToken();
        messageSender.joinMessage(replyToken);
    }

    private void handleTextContent(String replyToken, Event event, TextMessageContent content)
            throws Exception {
        String text = content.getText();

        log.info("Got text message from {}: {}", replyToken, text);

        if (text.charAt(0) == '/') {
            handleCommandMessage(replyToken, event, text);
            return;
        }

        switch (text) {
            case "profile": {
            }
            case "bye": {
                break;
            }
            default:
                break;
        }
    }

    private void handleCommandMessage(String replyToken, Event event, String text) {
        String[] splited = text.split("\\s+");
        switch (splited[0].toLowerCase()) {
            case "/list" :

                messageSender.replyText(replyToken, "Perintah belum tersedia");
                break;
            default:
                messageSender.replyText(replyToken, "Perintah tidak ditemukan");
        }

    }

}
