package com.aldiand.gitbot.controller;

import com.aldiand.gitbot.model.gitlab.Commit;
import com.aldiand.gitbot.model.gitlab.GitLabPushEvent;
import com.aldiand.gitbot.model.util.ThreadPoolFactory;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.ExecutionException;

@Slf4j
@RestController
@RequestMapping(value="/linebot")
public class RepoController {
    @Autowired
    private LineMessagingClient lineMessagingClient;

    @RequestMapping(value = "/message", method = RequestMethod.POST)
    public ResponseEntity<Boolean> sendMessage(HttpServletRequest request){
        String message = request.getParameter("message");
        String target = "C108f13658e948b6a37630b26e13bfa28";
        pushMessage(target, message);

        return new ResponseEntity<Boolean>(HttpStatus.OK);
    }

    @RequestMapping(value="/gitlab", method= RequestMethod.POST)
    public ResponseEntity<String> gitlabHook(@RequestHeader("X-Gitlab-Event") String event,
                                             @RequestBody(required = false) GitLabPushEvent pushEvent) {
        ThreadPoolFactory.getThreadPool().execute(() -> {
            switch (event) {
                case "Push Hook":
                    sendPushEvent(pushEvent);
                    break;
            }
        });
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    private void sendPushEvent(GitLabPushEvent pushEvent) {
        String target = "C108f13658e948b6a37630b26e13bfa28";
        String messageToUser;
        messageToUser = "New push\n" +
                "To: " + pushEvent.getRepository().getName() + "\n" +
                "On " + pushEvent.getRef() +  "\n" +
                "By " + pushEvent.getCommits().get(0).getAuthor().getName();

        pushMessage(target, messageToUser);

        String commitMessage;
        commitMessage = "Commit(s)\n";
        for (int i=0; i < pushEvent.getCommits().size(); i++) {
            Commit commit = pushEvent.getCommits().get(i);
            commitMessage = commitMessage + (i+1) + ". " + commit.getMessage() + "\n";
            if (commit.getAdded().size() != 0) {
                commitMessage = commitMessage + "Files added:\n";
                for(String s : commit.getAdded()) {
                    commitMessage = commitMessage + s + "\n";
                }
                commitMessage = commitMessage + "\n";
            }

            if (commit.getModified().size() != 0) {
                commitMessage = commitMessage + "Files modified:\n";
                for(String s : commit.getModified()) {
                    commitMessage = commitMessage + s + "\n";
                }
                commitMessage = commitMessage + "\n";
            }

            if (commit.getRemoved().size() != 0) {
                commitMessage = commitMessage + "Files modified:\n";
                for(String s : commit.getRemoved()) {
                    commitMessage = commitMessage + s + "\n";
                }
                commitMessage = commitMessage + "\n";
            }
            commitMessage = commitMessage + "" + "=============\n";
        }

        pushMessage(target, commitMessage);

    }


    private void pushMessage(String sourceId, String txt){
        TextMessage textMessage = new TextMessage(txt);
        PushMessage pushMessage = new PushMessage(sourceId,textMessage);
        try {
            BotApiResponse response = lineMessagingClient
                    .pushMessage(pushMessage)
                    .get();
            log.info("Sent messages: {}", response);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
