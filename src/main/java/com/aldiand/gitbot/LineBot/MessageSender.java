package com.aldiand.gitbot.LineBot;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Slf4j
@Component("messageSender")
public class MessageSender implements MessageInterface {

    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Override
    public void pushMessage(String sourceId, String txt) {
        List<Message> messages = new ArrayList<Message>();
        for(int j = 0; j <= txt.length() / 1999; j++) {
            messages.add(new TextMessage(txt.substring(j * 1999, Math.min((j + 1) * 1999, txt.length()))));
        }
        push(sourceId, messages);
    }

    @Override
    public void push(String sourceId, Message message) {
        push(sourceId, Collections.singletonList(message));
    }

    @Override
    public void push(String sourceId, List<Message> messages) {
        try {
            BotApiResponse response = lineMessagingClient
                    .pushMessage(new PushMessage(sourceId, messages))
                    .get();
            log.info("Sent messages: {}", response);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void replyText(String replyToken, String message) {
        if (replyToken.isEmpty()) {
            throw new IllegalArgumentException("replyToken must not be empty");
        }
        if (message.length() > 1000) {
            message = message.substring(0, 1000 - 2) + "……";
        }
        this.reply(replyToken, new TextMessage(message));
    }

    @Override
    public void reply(String replyToken, Message message) {
        reply(replyToken, Collections.singletonList(message));
    }

    @Override
    public void reply(String replyToken, List<Message> messages) {
        try {
            BotApiResponse apiResponse = lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, messages))
                    .get();
            log.info("Sent messages: {}", apiResponse);
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void followMessage(String replyToken) {
        replyText(replyToken, "Hai. saya Gita bot. Saya dapat membantu anda memantau tim anda");
    }

    @Override
    public void joinMessage(String replyToken) {
        replyText(replyToken, "Hai. saya Gita bot. Saya dapat membantu anda dan tim anda untuk bekerja sama.");
    }
}
