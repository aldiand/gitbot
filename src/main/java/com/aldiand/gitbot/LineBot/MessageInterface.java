package com.aldiand.gitbot.LineBot;

import com.linecorp.bot.model.message.Message;
import lombok.NonNull;

import java.util.List;

public interface MessageInterface {
    // BASE
    void pushMessage(@NonNull String sourceId,@NonNull String txt);
    void push(@NonNull String sourceId, @NonNull Message message);
    void push(@NonNull String sourceId, @NonNull List<Message> messages);
    void replyText(@NonNull String replyToken, @NonNull String message);
    void reply(@NonNull String replyToken, @NonNull Message message);
    void reply(@NonNull String replyToken, @NonNull List<Message> messages);

    // FEATURE
    void followMessage(@NonNull String replyToken);
    void joinMessage(@NonNull String replyToken);
}
