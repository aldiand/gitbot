package com.aldiand.gitbot.LineBot;

import com.linecorp.bot.model.event.Event;

public interface CommandInterface {
    void showList(String replyToken, Event event);
}
