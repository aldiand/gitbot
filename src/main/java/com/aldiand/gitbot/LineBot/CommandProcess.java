package com.aldiand.gitbot.LineBot;

import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.source.GroupSource;
import com.linecorp.bot.model.event.source.RoomSource;
import com.linecorp.bot.model.event.source.Source;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component("messageSender")
public class CommandProcess implements CommandInterface{
    @Autowired
    MessageSender messageSender;

    @Override
    public void showList(String replyToken, Event event) {
        Source source = event.getSource();
        if (source instanceof GroupSource) {
            this.replyText(replyToken, "Leaving group");
            lineMessagingClient.leaveGroup(((GroupSource) source).getGroupId()).get();
        } else if (source instanceof RoomSource) {
            this.replyText(replyToken, "Leaving room");
            lineMessagingClient.leaveRoom(((RoomSource) source).getRoomId()).get();
        } else {
            this.replyText(replyToken, "Bot can't leave from 1:1 chat");
        }
    }
}
