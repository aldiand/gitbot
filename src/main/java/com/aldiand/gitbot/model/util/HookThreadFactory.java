package com.aldiand.gitbot.model.util;

import java.util.concurrent.ThreadFactory;

public class HookThreadFactory  implements ThreadFactory {

    @Override
    public Thread newThread(Runnable r) {
        return new Thread(r);
    }

}
