package com.aldiand.gitbot.model.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolFactory {


    private static ExecutorService fixedThreadPool = Executors
            .newFixedThreadPool(10);

    public static ExecutorService getThreadPool() {
        return fixedThreadPool;
    }
}
