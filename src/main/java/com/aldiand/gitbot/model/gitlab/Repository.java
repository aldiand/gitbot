package com.aldiand.gitbot.model.gitlab;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Repository {

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("homepage")
    @Expose
    public String homepage;
    @SerializedName("git_http_url")
    @Expose
    public String gitHttpUrl;
    @SerializedName("git_ssh_url")
    @Expose
    public String gitSshUrl;
    @SerializedName("visibility_level")
    @Expose
    public Integer visibilityLevel;
}
