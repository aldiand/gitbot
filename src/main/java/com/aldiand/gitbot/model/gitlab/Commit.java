package com.aldiand.gitbot.model.gitlab;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class Commit {
    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("timestamp")
    @Expose
    public String timestamp;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("author")
    @Expose
    public Author author;
    @SerializedName("added")
    @Expose
    public List<String> added = null;
    @SerializedName("modified")
    @Expose
    public List<String> modified = null;
    @SerializedName("removed")
    @Expose
    public List<String> removed = null;
}
