package com.aldiand.gitbot.model.gitlab;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Project {
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("web_url")
    @Expose
    public String webUrl;
    @SerializedName("avatar_url")
    @Expose
    public Object avatarUrl;
    @SerializedName("git_ssh_url")
    @Expose
    public String gitSshUrl;
    @SerializedName("git_http_url")
    @Expose
    public String gitHttpUrl;
    @SerializedName("namespace")
    @Expose
    public String namespace;
    @SerializedName("visibility_level")
    @Expose
    public Integer visibilityLevel;
    @SerializedName("path_with_namespace")
    @Expose
    public String pathWithNamespace;
    @SerializedName("default_branch")
    @Expose
    public String defaultBranch;
    @SerializedName("homepage")
    @Expose
    public String homepage;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("ssh_url")
    @Expose
    public String sshUrl;
    @SerializedName("http_url")
    @Expose
    public String httpUrl;
}
