package com.aldiand.gitbot.model.gitlab;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
public class GitLabPushEvent {
    @SerializedName("object_kind")
    @Expose
    public String objectKind;
    @SerializedName("before")
    @Expose
    public String before;
    @SerializedName("after")
    @Expose
    public String after;
    @SerializedName("ref")
    @Expose
    public String ref;
    @SerializedName("checkout_sha")
    @Expose
    public String checkoutSha;
    @SerializedName("user_id")
    @Expose
    public Integer userId;
    @SerializedName("user_name")
    @Expose
    public String userName;
    @SerializedName("user_username")
    @Expose
    public String userUsername;
    @SerializedName("user_email")
    @Expose
    public String userEmail;
    @SerializedName("user_avatar")
    @Expose
    public String userAvatar;
    @SerializedName("project_id")
    @Expose
    public Integer projectId;
    @SerializedName("project")
    @Expose
    public Project project;
    @SerializedName("repository")
    @Expose
    public Repository repository;
    @SerializedName("commits")
    @Expose
    public List<Commit> commits = null;
    @SerializedName("total_commits_count")
    @Expose
    public Integer totalCommitsCount;
}
