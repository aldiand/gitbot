package com.aldiand.gitbot.model.github;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;

import java.util.List;

@Setter
public class PushEvent {
    @SerializedName("ref")
    @Expose
    public String ref;
    @SerializedName("before")
    @Expose
    public String before;
    @SerializedName("after")
    @Expose
    public String after;
    @SerializedName("created")
    @Expose
    public Boolean created;
    @SerializedName("deleted")
    @Expose
    public Boolean deleted;
    @SerializedName("forced")
    @Expose
    public Boolean forced;
    @SerializedName("base_ref")
    @Expose
    public Object baseRef;
    @SerializedName("compare")
    @Expose
    public String compare;
    @SerializedName("commits")
    @Expose
    public List<Object> commits = null;
    @SerializedName("head_commit")
    @Expose
    public Object headCommit;
    @SerializedName("repository")
    @Expose
    public Repository repository;
    @SerializedName("pusher")
    @Expose
    public Pusher pusher;
    @SerializedName("sender")
    @Expose
    public User sender;
}
