package com.aldiand.gitbot.model.line;

public class Source {
    public String userId;
    public String groupId;
    public String roomId;
    public String type;
}
