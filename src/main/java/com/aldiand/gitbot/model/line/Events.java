package com.aldiand.gitbot.model.line;

public class Events {
    public String type;
    public String replyToken;
    public Source source;
    public Long timestamp;
    public Message message;
}
