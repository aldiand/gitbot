package com.aldiand.gitbot.repository;

import com.aldiand.gitbot.model.db.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
}
