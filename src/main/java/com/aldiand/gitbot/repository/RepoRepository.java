package com.aldiand.gitbot.repository;

import com.aldiand.gitbot.model.db.Repo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface  RepoRepository  extends JpaRepository<Repo, Long> {
    List<Repo> findByUserId(String userId);
    List<Repo> findByGroupId(String groupId);
}
